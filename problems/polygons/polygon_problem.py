import multiprocessing
import random
import time
import traceback
import pickle as pk

from optimization.neighborhood import VNS, VND
from optimization.problem import Problem
from optimization.tabu import TabuSearch
import numpy as np
import matplotlib.pyplot as plt
import copy
import cv2


class PoligonProblem(Problem):
    def __init__(self, target_image, num_shapes, candidates_by_iteration=100,
                 max_edges = 10, delta=1, neighborhood = 'all',
                 polygon_list=None, sol_file='sol.png'):
        self.target_image = target_image
        self.target_image_diff = target_image.astype('int16')
        self.num_shapes = num_shapes
        self.candidates_by_iteration = candidates_by_iteration
        self.initilized_graph = False
        self.draw_queue = multiprocessing.Queue()
        self.draw_process = None
        self.polygon_list = polygon_list
        self.fitness_count = 0
        self.fitness_time = 0
        self.sol_file = sol_file
        self.max_edges = max_edges
        self.delta = delta
        self.neighborhood = neighborhood

    def get_neighborhood(self, cand, neighborhood=None, num_candidates=None):
        if neighborhood is None:
            neighborhood = self.neighborhood

        if num_candidates is None:
            num_candidates = self.candidates_by_iteration

        candidates = []
        while len(candidates) < num_candidates:
            if neighborhood == 'all':
                _neighborhood = random.choice(['move', 'color', 'add', 'remove'])
                candidates += self.get_neighborhood(cand, _neighborhood, 1)
            elif neighborhood == 'move':
                candidates.append(self.__move__neighbor(cand))
            elif neighborhood == 'color':
                candidates.append(self.__color_neighbor(cand))
            elif neighborhood == 'add':
                poligons = cand
                if self.polygon_list:
                    poligons = [cand[i] for i in self.polygon_list]
                if min([len(p) for p, c in poligons]) < self.max_edges:
                    new_cand = self.__add__neighbor(cand)
                    if not new_cand is None:
                        candidates.append(new_cand)
                else:
                    candidates.append(cand)
            elif neighborhood == 'remove':
                poligons = cand
                if self.polygon_list:
                    poligons = [cand[i] for i in self.polygon_list]
                if max([len(p) for p, c in poligons]) > 3:
                    new_cand = self.__remove__neighbor(cand)
                    if not new_cand is None:
                        candidates.append(new_cand)
                else:
                    candidates.append(cand)


        return candidates

    def __remove__neighbor(self, cand):
        raise Exception("Implementar")

    def __add__neighbor(self, cand):
        raise Exception("Implementar")

    def __color_neighbor(self, cand):
        raise Exception("Implementar")

    def __move__neighbor(self, cand):
        raise Exception("Implementar")

    def get_initial_solution(self):
        raise Exception("Implementar")

    def get_random_color(self):
        raise Exception("Implementar")

    def get_random_polygon(self):
        raise Exception("Implementar")

    def get_random_point(self, h, w):
        raise Exception("Implementar")

    def get_sol_diff(self, cand):
        h, w = self.target_image.shape[:2]
        sol = self.create_image_from_sol(cand)
        diff = self.target_image_diff - sol
        return diff

    def fitness(self, cand):
        start_time = time.time()
        h, w = self.target_image.shape[:2]
        diff = np.abs(self.get_sol_diff(cand)).sum()

        _fitness = (100 * diff / (w * h * 3 * 255)) ** 2

        self.fitness_time += (time.time() - start_time)
        self.fitness_count += 1

        return _fitness

    def create_image_from_sol(self, cand, to_rgb=True):
        h, w = self.target_image.shape[:2]
        sol = np.zeros((h, w, 4), np.uint8)
        #cand = [(((0, 0), (50, 0), (50, 20)), cand[0][1])]

        for shape, color in cand:
            overlay = sol.copy()

            pts = np.array(shape)
            pts = pts.reshape((-1, 1, 2))
            cv2.fillPoly(overlay, [pts], color)

            cv2.addWeighted(overlay, 0.8, sol, 0.2, 0, sol)

        if to_rgb:
            sol = cv2.cvtColor(sol, cv2.COLOR_RGBA2RGB)
        return sol

    def plot_sol(self, sol):

        if self.draw_process is None:
            self.draw_process = multiprocessing.Process(None, self.__plot_sol, args=(self.draw_queue,))
            self.draw_process.start()

        if self.draw_queue.qsize() < 1:
            self.draw_queue.put(sol)

    def __plot_sol(self, queue):


        if not self.initilized_graph:
            plt.ion()
            plt.show()
            self.initilized_graph = True

        w, h = self.target_image.shape[:2]
        while True:
            try:
                sol = queue.get_nowait()

                if sol == 'Q':
                    plt.close()
                    return

                sol = self.create_image_from_sol(sol, True)
                cv2.imwrite(self.sol_file, sol)

                try:
                    im = np.concatenate((cv2.cvtColor(sol, cv2.COLOR_RGB2BGR), cv2.cvtColor(self.target_image, cv2.COLOR_RGB2BGR)), axis=1)
                except:
                    traceback.print_exc()

                plt.clf()
                plt.imshow(im)
                #plt.imshow(cv2.cvtColor(sol, cv2.COLOR_RGB2BGR))
                #plt.draw()
                plt.pause(0.0001)

            except:

                plt.pause(0.1)

    def finish(self):
        self.draw_queue.put('Q')


if __name__ == '__main__':
    img = cv2.imread('data/images/mona-lisa-head.png')
    improving_list = []

    def improve(caller):
        global improving_list
        improving_list.append(caller.best)
        problem.plot_sol(caller.best)

    initial_solution = None
    i = 1
    current_fitness = 10000000
    problem = PoligonProblem(img, num_shapes=i, candidates_by_iteration=100,
                             delta=50,
                               sol_file='data/images/mona-lisa-head-sol2.png',
                             max_edges=7)

    while initial_solution is None or len(initial_solution) < 100:
        problem.num_shapes = i
        problem.polygon_list = [i-1]
        if not initial_solution is None:
            initial_solution.append((problem.get_random_polygon(), problem.get_random_color()))
        searcher = TabuSearch(problem, max_iterations=100,
                              list_length=2, improved_event=improve, tolerance=50)
        searcher.search(initial_solution=initial_solution)

        if current_fitness > searcher.best_fitness or initial_solution is None:
            initial_solution = searcher.best
            current_fitness = searcher.best_fitness

            if i > 1:
                problem.polygon_list = None
                #Aplicamos aquí VND (descenso por vecindades) o VNS
                pass


            i += 1
            print("Solution length: %d" % i)


        else:
            initial_solution = initial_solution[:-1]

        print("num fitness per second: %f" % (problem.fitness_count / problem.fitness_time))


    print("General optimization")
    problem.polygon_list=None
    searcher = TabuSearch(problem, max_iterations=10000, list_length=100,
                          improved_event=improve)
    searcher.search(initial_solution=initial_solution)
    problem.finish()

    pk.dump(improving_list, open("result/mona_lisa_%f.pk" % searcher.best_fitness, "wb"))

    print("Finish")
